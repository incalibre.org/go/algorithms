package tree

import (
	"fmt"
	"testing"
)

func TestMulti(t *testing.T) {
	for _, d := range multiTests {
		t.Run("inorder", func(t *testing.T) { d.testTraverse(t, d.tree.TraverseInorder, d.inorder) })
		t.Run("preorder", func(t *testing.T) { d.testTraverse(t, d.tree.TraversePreorder, d.preorder) })
		t.Run("postorder", func(t *testing.T) { d.testTraverse(t, d.tree.TraversePostorder, d.postorder) })
		t.Run("breadth", func(t *testing.T) { d.testTraverse(t, d.tree.TraverseBreadthFirst, d.breadth) })
	}
}

type multiTest struct {
	tree      *MultiNode
	inorder   []string
	preorder  []string
	postorder []string
	breadth   []string
}

var multiTests = []multiTest{
	{
		tree: &MultiNode{
			Value: 1,
			Left: []*MultiNode{
				{
					Value: 2,
					Left: []*MultiNode{
						{
							Value: 4,
						},
					},
					Right: []*MultiNode{
						{
							Value: 5,
						},
						{
							Value: 6,
						},
					},
				},
			},
			Right: []*MultiNode{
				{
					Value: 3,
				},
			},
		},
		inorder:   []string{"4", "2", "5", "6", "1", "3"},
		preorder:  []string{"1", "2", "4", "5", "6", "3"},
		postorder: []string{"4", "5", "6", "2", "3", "1"},
		breadth:   []string{"1", "2", "3", "4", "5", "6"},
	},
}

func (*multiTest) validate(t *testing.T, want []string, out <-chan interface{}) {
	got := make([]string, 0, len(want))
	for v := range out {
		got = append(got, fmt.Sprintf("%v", v))
	}

	if len(got) != len(want) {
		t.Errorf("got %d results, want %d: results %v, want %v", len(got), len(want), got, want)
		return
	}

	for i, r := range got {
		if r != want[i] {
			t.Errorf("result at index %d is invalid: results %v, want %v", i, got, want)
			return
		}
	}

	t.Log(got)
}

func (d *multiTest) testTraverse(t *testing.T, traverse func(chan<- interface{}), want []string) {
	out := make(chan interface{}, 1)
	go func() {
		traverse(out)
		close(out)
	}()
	d.validate(t, want, out)
}
