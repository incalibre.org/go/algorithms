package tree

type BinaryNode struct {
	Value interface{}
	Left  *BinaryNode
	Right *BinaryNode
}

// TraverseInorder is useful to traverse the tree in non-decreasing order.
func (n *BinaryNode) TraverseInorder(out chan<- interface{}) {
	if n == nil {
		return
	}
	n.Left.TraverseInorder(out)
	out <- n.Value
	n.Right.TraverseInorder(out)
}

// TraversePreorder is useful to copy the tree.
func (n *BinaryNode) TraversePreorder(out chan<- interface{}) {
	if n == nil {
		return
	}
	out <- n.Value
	n.Left.TraversePreorder(out)
	n.Right.TraversePreorder(out)
}

// TraversePostorder is useful to delete the tree.
func (n *BinaryNode) TraversePostorder(out chan<- interface{}) {
	if n == nil {
		return
	}
	n.Left.TraversePostorder(out)
	n.Right.TraversePostorder(out)
	out <- n.Value
}

// TraverseBreadthFirst is useful to traverse the tree by levels.
func (n *BinaryNode) TraverseBreadthFirst(out chan<- interface{}) {
	if n == nil {
		return
	}

	for order := []*BinaryNode{n}; len(order) > 0; {
		n := order[0]
		order = order[1:]

		out <- n.Value

		if n.Left != nil {
			order = append(order, n.Left)
		}
		if n.Right != nil {
			order = append(order, n.Right)
		}
	}
}
