package tree

type MultiNode struct {
	Value interface{}
	Left  []*MultiNode
	Right []*MultiNode
}

type traverser func(chan<- interface{})

func traverse(out chan<- interface{}, nodes []*MultiNode, f func(n *MultiNode) traverser) {
	for _, n := range nodes {
		_ = f(n)
		f(n)(out)
	}
}

// TraverseInorder is useful to traverse the tree in non-decreasing order.
func (n *MultiNode) TraverseInorder(out chan<- interface{}) {
	if n == nil {
		return
	}

	t := func(n *MultiNode) traverser { return n.TraverseInorder }
	traverse(out, n.Left, t)
	out <- n.Value
	traverse(out, n.Right, t)
}

// TraversePreorder is useful to copy the tree.
func (n *MultiNode) TraversePreorder(out chan<- interface{}) {
	if n == nil {
		return
	}

	t := func(n *MultiNode) traverser { return n.TraversePreorder }
	out <- n.Value
	traverse(out, n.Left, t)
	traverse(out, n.Right, t)
}

// TraversePostorder is useful to delete the tree.
func (n *MultiNode) TraversePostorder(out chan<- interface{}) {
	if n == nil {
		return
	}

	t := func(n *MultiNode) traverser { return n.TraversePostorder }
	traverse(out, n.Left, t)
	traverse(out, n.Right, t)
	out <- n.Value
}

// TraverseBreadthFirst is useful to traverse the tree by levels.
func (n *MultiNode) TraverseBreadthFirst(out chan<- interface{}) {
	if n == nil {
		return
	}

	for order := []*MultiNode{n}; len(order) > 0; {
		n := order[0]
		order = order[1:]

		out <- n.Value

		if n.Left != nil {
			order = append(order, n.Left...)
		}
		if n.Right != nil {
			order = append(order, n.Right...)
		}
	}
}
