// Code generated from quicksort.go using gen/gen.go; DO NOT EDIT.

package quicksort

import "gitlab.com/incalibre.org/go/algorithms/sort"

// ReorderBytes sorts []byte in place using quicksort.
func ReorderBytes(s []byte, o ...Option) { Reorder(sort.ByteSlice(s), o...) }

// ReorderFloat32s sorts []float32 in place using quicksort.
func ReorderFloat32s(s []float32, o ...Option) { Reorder(sort.Float32Slice(s), o...) }

// ReorderFloat64s sorts []float64 in place using quicksort.
func ReorderFloat64s(s []float64, o ...Option) { Reorder(sort.Float64Slice(s), o...) }

// ReorderInts sorts []int in place using quicksort.
func ReorderInts(s []int, o ...Option) { Reorder(sort.IntSlice(s), o...) }

// ReorderInt16s sorts []int16 in place using quicksort.
func ReorderInt16s(s []int16, o ...Option) { Reorder(sort.Int16Slice(s), o...) }

// ReorderInt32s sorts []int32 in place using quicksort.
func ReorderInt32s(s []int32, o ...Option) { Reorder(sort.Int32Slice(s), o...) }

// ReorderInt64s sorts []int64 in place using quicksort.
func ReorderInt64s(s []int64, o ...Option) { Reorder(sort.Int64Slice(s), o...) }

// ReorderInt8s sorts []int8 in place using quicksort.
func ReorderInt8s(s []int8, o ...Option) { Reorder(sort.Int8Slice(s), o...) }

// ReorderRunes sorts []rune in place using quicksort.
func ReorderRunes(s []rune, o ...Option) { Reorder(sort.RuneSlice(s), o...) }

// ReorderStrings sorts []string in place using quicksort.
func ReorderStrings(s []string, o ...Option) { Reorder(sort.StringSlice(s), o...) }

// ReorderUints sorts []uint in place using quicksort.
func ReorderUints(s []uint, o ...Option) { Reorder(sort.UintSlice(s), o...) }

// ReorderUint16s sorts []uint16 in place using quicksort.
func ReorderUint16s(s []uint16, o ...Option) { Reorder(sort.Uint16Slice(s), o...) }

// ReorderUint32s sorts []uint32 in place using quicksort.
func ReorderUint32s(s []uint32, o ...Option) { Reorder(sort.Uint32Slice(s), o...) }

// ReorderUint64s sorts []uint64 in place using quicksort.
func ReorderUint64s(s []uint64, o ...Option) { Reorder(sort.Uint64Slice(s), o...) }

// ReorderUint8s sorts []uint8 in place using quicksort.
func ReorderUint8s(s []uint8, o ...Option) { Reorder(sort.Uint8Slice(s), o...) }
