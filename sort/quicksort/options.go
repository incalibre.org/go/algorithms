package quicksort

import (
	"math/rand"
	"sync"
	"time"
)

// Option may be used to adjust the quicksort behavior.
type Option interface {
	apply(*quicksort)
}

type option struct {
	applyFunc func(*quicksort)
}

func (o *option) apply(q *quicksort) { o.applyFunc(q) }

// Reverse reverses the ordering of the quicksort.
// What do you think will happen if this option is applied more than once?
func Reverse() Option {
	return &option{
		applyFunc: func(q *quicksort) {
			l := q.less
			q.less = func(index1, index2 int) bool { return l(index2, index1) }
		},
	}
}

// PivotOnLeft will select the quicksort pivot point on the left.
func PivotOnLeft() Option {
	return &option{
		applyFunc: func(q *quicksort) {
			q.selectPivot = func(firstIndex, _ /* lastIndex */ int) int {
				return firstIndex
			}
		},
	}
}

// PivotOnRight will select the quicksort pivot point on the right.
func PivotOnRight() Option {
	return &option{
		applyFunc: func(q *quicksort) {
			q.selectPivot = func(_ /* firstIndex */, lastIndex int) int {
				return lastIndex
			}
		},
	}
}

// Used to initialize the random seed for random pivot point selection.
var seedRandOnce sync.Once

// PivotRandom will select the quicksort pivot point randomly.
func PivotRandom() Option {
	seedRandOnce.Do(func() { rand.Seed(time.Now().UnixNano()) })

	return &option{
		applyFunc: func(q *quicksort) {
			q.selectPivot = func(firstIndex, lastIndex int) int {
				spread := lastIndex - firstIndex
				if spread <= 0 {
					return lastIndex
				}
				return firstIndex + rand.Intn(spread+1)
			}
		},
	}
}
