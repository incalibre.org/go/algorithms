// build ignore

// Invoked via "go generate" to generate quicksort helper functions.

package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	golangsort "sort"
	"strings"

	"gitlab.com/incalibre.org/go/algorithms/sort"
)

// outputFlag specifies the output file name.
var outputFlag = flag.String("output", "helpers.go", "the output file name")

func main() {
	flag.Parse()

	g := &generate{
		fileName: *outputFlag,
		out:      new(bytes.Buffer),
	}

	if err := g.genHeader(); err != nil {
		log.Fatalf("failed to generate header: %v\n", err)
	}
	if err := g.genHelpers(); err != nil {
		log.Fatalf("failed to generate helpers: %v\n", err)
	}
	if err := g.write(); err != nil {
		log.Fatalf("failed to write: %v", err)
	}
}

type generate struct {
	fileName string
	out      *bytes.Buffer
}

func (g *generate) write() error {
	if g.fileName != "" {
		if err := ioutil.WriteFile(g.fileName, g.out.Bytes(), 0644); err != nil {
			return fmt.Errorf("file %q: %v", g.fileName, err)
		}
		return nil
	}

	if _, err := os.Stdout.Write(g.out.Bytes()); err != nil {
		return fmt.Errorf("<stdout>: %v", err)
	}
	return nil
}

func (g *generate) genHeader() error {
	if _, err := fmt.Fprintf(g.out, "// Code generated from quicksort.go using gen/gen.go; DO NOT EDIT.\n"); err != nil {
		return err
	}

	if _, err := fmt.Fprintf(g.out, "\npackage quicksort\n"); err != nil {
		return err
	}

	if _, err := fmt.Fprintf(g.out, "\nimport %q\n", "gitlab.com/incalibre.org/go/algorithms/sort"); err != nil {
		return err
	}

	return nil
}

func (g *generate) genHelpers() error {
	// Generate helpers sorted by the type names.
	typeNames := make([]string, 0, len(sort.GeneratedTypes))
	for typeName := range sort.GeneratedTypes {
		typeNames = append(typeNames, typeName)
	}
	golangsort.Strings(typeNames)

	for _, typeName := range typeNames {
		generatedName := "sort." + sort.GeneratedTypes[typeName]
		funcName := "Reorder" + strings.Title(typeName) + "s"

		if _, err := fmt.Fprintf(g.out, "\n// %s sorts []%s in place using quicksort.\n", funcName, typeName); err != nil {
			return err
		}
		if _, err := fmt.Fprintf(g.out, "func %s(s []%s, o ...Option) { Reorder(%s(s), o...) }\n", funcName, typeName, generatedName); err != nil {
			return err
		}
	}

	return nil
}
