package quicksort

import (
	"fmt"
	golangsort "sort"
	"testing"
)

type testStringsCase struct {
	values []string
}

func TestStrings(t *testing.T) {
	testCases := []testStringsCase{
		{[]string{"a", "", "x", "b"}},
		{},
		{[]string{}},
		{[]string{""}},
		{[]string{"one"}},
		{[]string{"one", "two"}},
		{[]string{"a", "c", "b"}},
		{[]string{"c", "b", "a"}},
		{[]string{"a", "b", "c"}},
	}

	for testIndex, testCase := range testCases {
		t.Run(fmt.Sprintf("%02d", testIndex), testCase.test)
	}
}

func (c *testStringsCase) test(t *testing.T) {
	cloneOriginal := func() []string { return append(make([]string, 0, len(c.values)), c.values...) }

	wantAscending := cloneOriginal()
	golangsort.Strings(wantAscending)
	wantDescending := cloneOriginal()
	golangsort.Sort(golangsort.Reverse(golangsort.StringSlice(wantDescending)))

	// Reorder in ascending order.
	t.Run("sorted", func(t *testing.T) { c.validate(t, cloneOriginal(), wantAscending) })
	t.Run("sorted PivotOnRight", func(t *testing.T) { c.validate(t, cloneOriginal(), wantAscending, PivotOnRight()) })
	t.Run("sorted PivotOnLeft", func(t *testing.T) { c.validate(t, cloneOriginal(), wantAscending, PivotOnLeft()) })
	t.Run("sorted PivotRandom", func(t *testing.T) { c.validate(t, cloneOriginal(), wantAscending, PivotRandom()) })

	// Reorder in descending order.
	t.Run("reverse", func(t *testing.T) { c.validate(t, cloneOriginal(), wantDescending, Reverse()) })
	t.Run("reverse PivotOnRight", func(t *testing.T) { c.validate(t, cloneOriginal(), wantDescending, Reverse(), PivotOnRight()) })
	t.Run("reverse PivotOnLeft", func(t *testing.T) { c.validate(t, cloneOriginal(), wantDescending, Reverse(), PivotOnLeft()) })
	t.Run("reverse PivotRandom", func(t *testing.T) { c.validate(t, cloneOriginal(), wantDescending, Reverse(), PivotRandom()) })

	// See what happens when the Reverse option is used twice.
	t.Run("reverse twice", func(t *testing.T) { c.validate(t, cloneOriginal(), wantAscending, Reverse(), Reverse()) })
	t.Run("reverse twice PivotOnRight", func(t *testing.T) {
		c.validate(t, cloneOriginal(), wantAscending, Reverse(), Reverse(), PivotOnRight())
	})
	t.Run("reverse twice PivotOnLeft", func(t *testing.T) { c.validate(t, cloneOriginal(), wantAscending, Reverse(), Reverse(), PivotOnLeft()) })
	t.Run("reverse twice PivotRandom", func(t *testing.T) { c.validate(t, cloneOriginal(), wantAscending, Reverse(), Reverse(), PivotRandom()) })
}

func (*testStringsCase) validate(t *testing.T, got []string, want []string, option ...Option) {
	ReorderStrings(got, option...)
	if len(got) != len(want) {
		t.Fatalf("failed: want length %d, got length %d: want %v, got %v", len(want), len(got), want, got)
	}
	for index := 0; index < len(got); index++ {
		if got[index] != want[index] {
			t.Fatalf("failed: want %v, got %v", want, got)
		}
	}
}
