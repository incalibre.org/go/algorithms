//go:generate go run gen/gen.go

// Package quicksort provides a quicksort algorithm implementation.
package quicksort

import (
	"gitlab.com/incalibre.org/go/algorithms/sort"
)

// Reorder reorders a Sortable in place using quicksort.
func Reorder(sortable sort.Sortable, options ...Option) {
	if sortable == nil {
		panic("sortable is nil")
	}

	ReorderBy(sortable, sortable.Less, options...)
}

// ReorderBy reorders an Ordered in place using quicksort.
// The less function parameter returns true if the element at index1 should be
// ordered before the element at index 2.
func ReorderBy(ordered sort.Ordered, less func(index1, index2 int) bool, options ...Option) {
	if ordered == nil {
		panic("ordered is nil")
	}
	if less == nil {
		panic("less is nill")
	}

	q := &quicksort{
		ordered: ordered,
		less:    less,
	}

	for _, option := range options {
		option.apply(q)
	}

	if q.selectPivot == nil {
		PivotOnRight().apply(q)
	}

	q.sort(0, q.ordered.Len()-1)
}

type quicksort struct {
	ordered     sort.Ordered
	less        func(index1, index2 int) bool
	selectPivot func(firstIndex, lastIndex int) int
}

func (q *quicksort) sort(firstIndex, lastIndex int) {
	if lastIndex <= firstIndex {
		return
	}

	pivotIndex := q.pivot(firstIndex, lastIndex)
	q.sort(firstIndex, pivotIndex-1)
	q.sort(pivotIndex+1, lastIndex)
}

func (q *quicksort) pivot(firstIndex, lastIndex int) int {
	// Select the pivot index and then swap the pivot index value with the
	// last index value to have the pivot value at the last index.
	q.ordered.Swap(q.selectPivot(firstIndex, lastIndex), lastIndex)

	// Swap all the elements which are less than the last index value to be at
	// the smallest indices of the collection.
	for elementIndex := firstIndex; elementIndex < lastIndex; elementIndex++ {
		if q.less(elementIndex, lastIndex) {
			q.ordered.Swap(firstIndex, elementIndex)
			firstIndex++
		}
	}
	q.ordered.Swap(firstIndex, lastIndex)
	return firstIndex
}
