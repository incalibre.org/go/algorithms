package quicksort

import (
	"fmt"
	golangsort "sort"
	"testing"
)

func TestInts(t *testing.T) {
	testCases := []testIntsCase{
		{[]int{4, 2, 6, -1, 0}},
		{},
		{[]int{1}},
		{[]int{1, 2}},
		{[]int{1, 3, 2}},
		{[]int{3, 2, 1}},
		{[]int{1, 2, 3}},
		{[]int{1, 1, 1}},
		{[]int{1, 1, 1, 2, 2, 2}},
		{[]int{2, 2, 2, 1, 1, 1}},
		{[]int{1, 2, 1, 2, 1, 2}},
		{[]int{2, 1, 2, 1, 2, 1}},
	}

	for testIndex, testCase := range testCases {
		t.Run(fmt.Sprintf("%02d", testIndex), testCase.test)
	}
}

type testIntsCase struct {
	values []int
}

func (c *testIntsCase) test(t *testing.T) {
	cloneOriginal := func() []int { return append(make([]int, 0, len(c.values)), c.values...) }

	wantAscending := cloneOriginal()
	golangsort.Ints(wantAscending)
	wantDescending := cloneOriginal()
	golangsort.Sort(golangsort.Reverse(golangsort.IntSlice(wantDescending)))

	// Reorder in ascending order.
	t.Run("sorted", func(t *testing.T) { c.validate(t, cloneOriginal(), wantAscending) })
	t.Run("sorted PivotOnRight", func(t *testing.T) { c.validate(t, cloneOriginal(), wantAscending, PivotOnRight()) })
	t.Run("sorted PivotOnLeft", func(t *testing.T) { c.validate(t, cloneOriginal(), wantAscending, PivotOnLeft()) })
	t.Run("sorted PivotRandom", func(t *testing.T) { c.validate(t, cloneOriginal(), wantAscending, PivotRandom()) })

	// Reorder in descending order.
	t.Run("reverse", func(t *testing.T) { c.validate(t, cloneOriginal(), wantDescending, Reverse()) })
	t.Run("reverse PivotOnRight", func(t *testing.T) { c.validate(t, cloneOriginal(), wantDescending, Reverse(), PivotOnRight()) })
	t.Run("reverse PivotOnLeft", func(t *testing.T) { c.validate(t, cloneOriginal(), wantDescending, Reverse(), PivotOnLeft()) })
	t.Run("reverse PivotRandom", func(t *testing.T) { c.validate(t, cloneOriginal(), wantDescending, Reverse(), PivotRandom()) })

	// See what happens when the Reverse option is used twice.
	t.Run("reverse twice", func(t *testing.T) { c.validate(t, cloneOriginal(), wantAscending, Reverse(), Reverse()) })
	t.Run("reverse twice PivotOnRight", func(t *testing.T) {
		c.validate(t, cloneOriginal(), wantAscending, Reverse(), Reverse(), PivotOnRight())
	})
	t.Run("reverse twice PivotOnLeft", func(t *testing.T) { c.validate(t, cloneOriginal(), wantAscending, Reverse(), Reverse(), PivotOnLeft()) })
	t.Run("reverse twice PivotRandom", func(t *testing.T) { c.validate(t, cloneOriginal(), wantAscending, Reverse(), Reverse(), PivotRandom()) })
}

func (*testIntsCase) validate(t *testing.T, got []int, want []int, option ...Option) {
	ReorderInts(got, option...)
	if len(got) != len(want) {
		t.Fatalf("failed: want length %d, got length %d: want %v, got %v", len(want), len(got), want, got)
	}
	for index := 0; index < len(got); index++ {
		if got[index] != want[index] {
			t.Fatalf("failed: want %v, got %v", want, got)
		}
	}
}
