package quicksort

import (
	"gitlab.com/incalibre.org/go/algorithms/sort"
)

// ExampleReorder provides an example of using generated Sortable types.
// To sort types which do not have generated Sortable types, implement the
// Sortable interface for the type.
// See an example of how OrderedStrings is declared.
func ExampleReorder() {
	// To sort slices of strings and other builtin types with predefined Ordered
	// types, use the following pattern.
	values := []string{"a", "x", "b"}

	// Reorder can take any Sortable type.
	Reorder(sort.StringSlice(values))

	// Generated shorthand for Sortable types.
	ReorderStrings(values)

	// Sorting in reverse is easy by using options.
	ReorderStrings(values, Reverse())

	// Using the reverse option twice, produces a surprising result...
	ReorderStrings(values, Reverse(), Reverse())

	// Pivot selection may be important when data is mostly ordered.
	ReorderStrings(values, PivotRandom())
	ReorderStrings(values, PivotOnLeft())
	ReorderStrings(values, PivotOnRight())

	// Of course you can always define your own Sortable types.
	// See OrderedStrings, OrderedInts, etc. for an example.
}
