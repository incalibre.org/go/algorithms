// Code generated from sort.go using gen/gen.go; DO NOT EDIT.

package sort

// GeneratedTypes are the generated types implementing Sortable.
var GeneratedTypes = map[string]string{
	"byte":    "ByteSlice",
	"float32": "Float32Slice",
	"float64": "Float64Slice",
	"int":     "IntSlice",
	"int16":   "Int16Slice",
	"int32":   "Int32Slice",
	"int64":   "Int64Slice",
	"int8":    "Int8Slice",
	"rune":    "RuneSlice",
	"string":  "StringSlice",
	"uint":    "UintSlice",
	"uint16":  "Uint16Slice",
	"uint32":  "Uint32Slice",
	"uint64":  "Uint64Slice",
	"uint8":   "Uint8Slice",
}

// ByteSlice attaches the methods of Sortable to []byte, to sort in increasing order.
type ByteSlice []byte

// Len returns the number of elements.
func (s ByteSlice) Len() int { return len(s) }

// Swap swaps the elements at index1 and index2.
func (s ByteSlice) Swap(index1, index2 int) { s[index1], s[index2] = s[index2], s[index1] }

// Less returns true if the element at index1 should be ordered before the element at index2.
func (s ByteSlice) Less(index1, index2 int) bool { return s[index1] < s[index2] }

// Float32Slice attaches the methods of Sortable to []float32, to sort in increasing order.
type Float32Slice []float32

// Len returns the number of elements.
func (s Float32Slice) Len() int { return len(s) }

// Swap swaps the elements at index1 and index2.
func (s Float32Slice) Swap(index1, index2 int) { s[index1], s[index2] = s[index2], s[index1] }

// Less returns true if the element at index1 should be ordered before the element at index2.
func (s Float32Slice) Less(index1, index2 int) bool { return s[index1] < s[index2] }

// Float64Slice attaches the methods of Sortable to []float64, to sort in increasing order.
type Float64Slice []float64

// Len returns the number of elements.
func (s Float64Slice) Len() int { return len(s) }

// Swap swaps the elements at index1 and index2.
func (s Float64Slice) Swap(index1, index2 int) { s[index1], s[index2] = s[index2], s[index1] }

// Less returns true if the element at index1 should be ordered before the element at index2.
func (s Float64Slice) Less(index1, index2 int) bool { return s[index1] < s[index2] }

// IntSlice attaches the methods of Sortable to []int, to sort in increasing order.
type IntSlice []int

// Len returns the number of elements.
func (s IntSlice) Len() int { return len(s) }

// Swap swaps the elements at index1 and index2.
func (s IntSlice) Swap(index1, index2 int) { s[index1], s[index2] = s[index2], s[index1] }

// Less returns true if the element at index1 should be ordered before the element at index2.
func (s IntSlice) Less(index1, index2 int) bool { return s[index1] < s[index2] }

// Int16Slice attaches the methods of Sortable to []int16, to sort in increasing order.
type Int16Slice []int16

// Len returns the number of elements.
func (s Int16Slice) Len() int { return len(s) }

// Swap swaps the elements at index1 and index2.
func (s Int16Slice) Swap(index1, index2 int) { s[index1], s[index2] = s[index2], s[index1] }

// Less returns true if the element at index1 should be ordered before the element at index2.
func (s Int16Slice) Less(index1, index2 int) bool { return s[index1] < s[index2] }

// Int32Slice attaches the methods of Sortable to []int32, to sort in increasing order.
type Int32Slice []int32

// Len returns the number of elements.
func (s Int32Slice) Len() int { return len(s) }

// Swap swaps the elements at index1 and index2.
func (s Int32Slice) Swap(index1, index2 int) { s[index1], s[index2] = s[index2], s[index1] }

// Less returns true if the element at index1 should be ordered before the element at index2.
func (s Int32Slice) Less(index1, index2 int) bool { return s[index1] < s[index2] }

// Int64Slice attaches the methods of Sortable to []int64, to sort in increasing order.
type Int64Slice []int64

// Len returns the number of elements.
func (s Int64Slice) Len() int { return len(s) }

// Swap swaps the elements at index1 and index2.
func (s Int64Slice) Swap(index1, index2 int) { s[index1], s[index2] = s[index2], s[index1] }

// Less returns true if the element at index1 should be ordered before the element at index2.
func (s Int64Slice) Less(index1, index2 int) bool { return s[index1] < s[index2] }

// Int8Slice attaches the methods of Sortable to []int8, to sort in increasing order.
type Int8Slice []int8

// Len returns the number of elements.
func (s Int8Slice) Len() int { return len(s) }

// Swap swaps the elements at index1 and index2.
func (s Int8Slice) Swap(index1, index2 int) { s[index1], s[index2] = s[index2], s[index1] }

// Less returns true if the element at index1 should be ordered before the element at index2.
func (s Int8Slice) Less(index1, index2 int) bool { return s[index1] < s[index2] }

// RuneSlice attaches the methods of Sortable to []rune, to sort in increasing order.
type RuneSlice []rune

// Len returns the number of elements.
func (s RuneSlice) Len() int { return len(s) }

// Swap swaps the elements at index1 and index2.
func (s RuneSlice) Swap(index1, index2 int) { s[index1], s[index2] = s[index2], s[index1] }

// Less returns true if the element at index1 should be ordered before the element at index2.
func (s RuneSlice) Less(index1, index2 int) bool { return s[index1] < s[index2] }

// StringSlice attaches the methods of Sortable to []string, to sort in increasing order.
type StringSlice []string

// Len returns the number of elements.
func (s StringSlice) Len() int { return len(s) }

// Swap swaps the elements at index1 and index2.
func (s StringSlice) Swap(index1, index2 int) { s[index1], s[index2] = s[index2], s[index1] }

// Less returns true if the element at index1 should be ordered before the element at index2.
func (s StringSlice) Less(index1, index2 int) bool { return s[index1] < s[index2] }

// UintSlice attaches the methods of Sortable to []uint, to sort in increasing order.
type UintSlice []uint

// Len returns the number of elements.
func (s UintSlice) Len() int { return len(s) }

// Swap swaps the elements at index1 and index2.
func (s UintSlice) Swap(index1, index2 int) { s[index1], s[index2] = s[index2], s[index1] }

// Less returns true if the element at index1 should be ordered before the element at index2.
func (s UintSlice) Less(index1, index2 int) bool { return s[index1] < s[index2] }

// Uint16Slice attaches the methods of Sortable to []uint16, to sort in increasing order.
type Uint16Slice []uint16

// Len returns the number of elements.
func (s Uint16Slice) Len() int { return len(s) }

// Swap swaps the elements at index1 and index2.
func (s Uint16Slice) Swap(index1, index2 int) { s[index1], s[index2] = s[index2], s[index1] }

// Less returns true if the element at index1 should be ordered before the element at index2.
func (s Uint16Slice) Less(index1, index2 int) bool { return s[index1] < s[index2] }

// Uint32Slice attaches the methods of Sortable to []uint32, to sort in increasing order.
type Uint32Slice []uint32

// Len returns the number of elements.
func (s Uint32Slice) Len() int { return len(s) }

// Swap swaps the elements at index1 and index2.
func (s Uint32Slice) Swap(index1, index2 int) { s[index1], s[index2] = s[index2], s[index1] }

// Less returns true if the element at index1 should be ordered before the element at index2.
func (s Uint32Slice) Less(index1, index2 int) bool { return s[index1] < s[index2] }

// Uint64Slice attaches the methods of Sortable to []uint64, to sort in increasing order.
type Uint64Slice []uint64

// Len returns the number of elements.
func (s Uint64Slice) Len() int { return len(s) }

// Swap swaps the elements at index1 and index2.
func (s Uint64Slice) Swap(index1, index2 int) { s[index1], s[index2] = s[index2], s[index1] }

// Less returns true if the element at index1 should be ordered before the element at index2.
func (s Uint64Slice) Less(index1, index2 int) bool { return s[index1] < s[index2] }

// Uint8Slice attaches the methods of Sortable to []uint8, to sort in increasing order.
type Uint8Slice []uint8

// Len returns the number of elements.
func (s Uint8Slice) Len() int { return len(s) }

// Swap swaps the elements at index1 and index2.
func (s Uint8Slice) Swap(index1, index2 int) { s[index1], s[index2] = s[index2], s[index1] }

// Less returns true if the element at index1 should be ordered before the element at index2.
func (s Uint8Slice) Less(index1, index2 int) bool { return s[index1] < s[index2] }
