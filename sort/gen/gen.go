// build ignore

// Invoked via "go generate" to generate helper types and functions.

package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strings"
)

// outputFlag specifies the output file name.
var outputFlag = flag.String("output", "sortables.go", "the output file name")

func main() {
	flag.Parse()

	g := &generate{
		fileName: *outputFlag,
		out:      new(bytes.Buffer),
		typeNames: []string{
			"string",
			"int8",
			"uint8",
			"byte",
			"int16",
			"uint16",
			"int32",
			"rune",
			"uint32",
			"int64",
			"uint64",
			"int",
			"uint",
			"float32",
			"float64",
		},
	}
	sort.Strings(g.typeNames)

	if err := g.genHeader(); err != nil {
		log.Fatalf("failed to generate header: %v\n", err)
	}
	if err := g.genTypeNames(); err != nil {
		log.Fatalf("failed to generate type names: %v\n", err)
	}
	if err := g.genSortableTypes(); err != nil {
		log.Fatalf("failed to generate sortable types: %v\n", err)
	}
	if err := g.write(); err != nil {
		log.Fatalf("failed to write: %v", err)
	}
}

type generate struct {
	fileName  string
	out       *bytes.Buffer
	typeNames []string
}

func (g *generate) write() error {
	if g.fileName != "" {
		if err := ioutil.WriteFile(g.fileName, g.out.Bytes(), 0644); err != nil {
			return fmt.Errorf("file %q: %v", g.fileName, err)
		}
		return nil
	}

	if _, err := os.Stdout.Write(g.out.Bytes()); err != nil {
		return fmt.Errorf("<stdout>: %v", err)
	}
	return nil
}

func (g *generate) genHeader() error {
	if _, err := fmt.Fprintf(g.out, "// Code generated from sort.go using gen/gen.go; DO NOT EDIT.\n\npackage sort\n"); err != nil {
		return err
	}

	return nil
}

func (g *generate) sortableName(typeName string) string {
	return strings.Title(typeName) + "Slice"
}

func (g *generate) genTypeNames() error {
	if _, err := fmt.Fprintf(g.out, "\n// GeneratedTypes are the generated types implementing Sortable.\n"); err != nil {
		return err
	}
	if _, err := fmt.Fprintf(g.out, "var GeneratedTypes = map[string]string{\n"); err != nil {
		return err
	}

	for _, typeName := range g.typeNames {
		if _, err := fmt.Fprintf(g.out, "  %q: %q,\n", typeName, g.sortableName(typeName)); err != nil {
			return err
		}
	}

	if _, err := fmt.Fprintf(g.out, "}\n"); err != nil {
		return err
	}

	return nil
}

func (g *generate) genSortableTypes() error {
	for _, typeName := range g.typeNames {
		sortableName := g.sortableName(typeName)

		if _, err := fmt.Fprintf(g.out, "\n// %s attaches the methods of Sortable to []%s, to sort in increasing order.\n", sortableName, typeName); err != nil {
			return err
		}
		if _, err := fmt.Fprintf(g.out, "type %s []%s\n", sortableName, typeName); err != nil {
			return err
		}

		if _, err := fmt.Fprintf(g.out, "\n// Len returns the number of elements.\n"); err != nil {
			return err
		}
		if _, err := fmt.Fprintf(g.out, "func (s %s) Len() int { return len(s) }\n", sortableName); err != nil {
			return err
		}

		if _, err := fmt.Fprintf(g.out, "\n// Swap swaps the elements at index1 and index2.\n"); err != nil {
			return err
		}
		if _, err := fmt.Fprintf(g.out, "func (s %s) Swap(index1, index2 int) { s[index1], s[index2] = s[index2], s[index1] }\n", sortableName); err != nil {
			return err
		}

		if _, err := fmt.Fprintf(g.out, "\n// Less returns true if the element at index1 should be ordered before the element at index2.\n"); err != nil {
			return err
		}
		if _, err := fmt.Fprintf(g.out, "func (s %s) Less(index1, index2 int) bool { return s[index1] < s[index2] }\n", sortableName); err != nil {
			return err
		}
	}

	return nil
}
