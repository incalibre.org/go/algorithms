//go:generate go run gen/gen.go

// Package sort provides utilities for sorting slices and user-defined
// collections.
package sort

// Ordered is implemented by ordered types such as slices or lists
// with a known length, and elements which may be swapped using their indices.
type Ordered interface {
	// Returns the number of elements.
	Len() int

	// Swaps elements at two indices.
	Swap(index1, index2 int)
}

// Sortable is implemented by ordered types which may be sorted.
type Sortable interface {
	Ordered

	// Less returns true if the element at index1 should be ordered before the
	// element at index2.
	Less(index1, index2 int) bool
}
